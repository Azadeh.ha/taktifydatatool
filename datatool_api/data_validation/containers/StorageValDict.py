from ..attribute import Attribute
from typing import MutableMapping, Generic, Iterator, ItemsView, KeysView, ValuesView, Optional
from typing import Mapping, Tuple
from typing import TypeVar


_T = TypeVar("_T")
_KT = TypeVar("_KT")
_VT = TypeVar("_VT")
_VT_co = TypeVar("_VT_co", covariant=True)
_T_co = TypeVar("_T_co", covariant=True)


class StorageValDict(MutableMapping[_KT, _VT], Generic[_KT, _VT]):
    def __init__(self, validator: Attribute = None, name: str = None, parent: object = None):
        self._name = name
        self._parent = parent
        self._validator = validator
        self._data = {}

    def _get_storage_interface(self):
        if self._parent is None or getattr(self._parent, '_storageInterface', None) is None:
            raise ValueError('Parent object must be set in StorageValList and must have a set _storageInterface '
                             'attribute pointing to StorageIOInterface')
        else:
            return getattr(self._parent, '_storageInterface')

    def __setitem__(self, k: _KT, v: _VT) -> None:
        if self._validator is not None:
            self._validator.validate(str(k), v)
        pointer = self._get_storage_interface().write_to_storage(
            str(k), (self._parent.__class__.__name__ + self._name), v)
        self._data[k] = pointer
        if (self._parent and self._name) is not None and getattr(self._parent, self._name, None) is None:
            setattr(self._parent, self._name, self)

    def __getitem__(self, k: _KT) -> _VT_co:
        pointer = self._data[k]
        return self._get_storage_interface().read_from_storage(pointer)

    def get(self, key: _KT, default: _VT_co = None) -> Optional[_VT_co]:
        pointer = self._data.get(key, None)
        if pointer is not None:
            return self._get_storage_interface().read_from_storage(pointer)
        return default

    def __len__(self) -> int:
        return len(self._data)

    def __iter__(self) -> Iterator[_T_co]:
        return iter(self._data)

    def __contains__(self, key: _KT) -> bool:
        return key in self._data

    def items(self) -> ItemsView[_KT, _VT]:
        for k, v in self._data.items():
            pointer = self._data[k]
            yield k, self._get_storage_interface().read_from_storage(pointer)

    def keys(self) -> KeysView[_KT]:
        return self._data.keys()

    def values(self) -> ValuesView[_VT]:
        for v in self._data.values():
            yield self._get_storage_interface().read_from_storage(v)

    def pop(self, key: _KT, default: _VT = None) -> _VT:
        pointer = self._data.pop(key, None)
        if pointer is not None:
            return self._get_storage_interface().read_from_storage(pointer, remove=True)
        return default

    def has_key(self, key: _KT) -> bool:
        return key in self._data

    def add(self, k: _KT, v: _VT) -> None:
        self.__setitem__(k, v)

    def item_count(self) -> int:
        return len(self._data)

    def clear(self) -> None:
        for v in self._data.values():
            self._get_storage_interface().remove_from_storage(v)
        self._data.clear()

    def popitem(self) -> Tuple[_KT, _VT]:
        k, v = self._data.popitem()
        return k, self._get_storage_interface().read_from_storage(v, remove=True)

    def __delitem__(self, v: _KT) -> None:
        pointer = self._data[v]
        self._get_storage_interface().remove_from_storage(pointer)
        self._data.__delitem__(v)

    def setdefault(self, key: _KT, default: _VT = ...) -> _VT:
        raise NotImplementedError

    def update(self, __m: Mapping[_KT, _VT], **kwargs: _VT) -> None:
        raise NotImplementedError
