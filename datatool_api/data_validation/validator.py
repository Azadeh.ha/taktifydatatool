from datetime import datetime
from typing import MutableMapping, MutableSequence


class Validators:
    @staticmethod
    def check_type(name, value, target):
        if not isinstance(value, target):
            raise TypeError('Wrong type for {}, should be {}'.format(name, target))

    @staticmethod
    def check_less_than(name, value, target):
        if not value < target:
            raise ValueError('{} should be less than {}'.format(name, target))

    @staticmethod
    def check_greater_than(name, value, target):
        if not value > target:
            raise ValueError('{} should be greater than {}'.format(name, target))

    @staticmethod
    def check_less_than_equal(name, value, target):
        if not value <= target:
            raise ValueError('{} should be less than equal to {}'.format(name, target))

    @staticmethod
    def check_greater_than_equal(name, value, target):
        if not value >= target:
            raise ValueError('{} should be greater than equal to {}'.format(name, target))

    @staticmethod
    def check_equal(name, value, target):
        if not value == target:
            raise ValueError('{} should be equal to {}'.format(name, target))

    @staticmethod
    def check_not_equal(name, value, target):
        if not value != target:
            raise ValueError('{} should not be equal to {}'.format(name, target))

    @staticmethod
    def check_min_items(name, value, target):
        if not ((isinstance(value, (list, MutableSequence)) or isinstance(value, (dict, MutableMapping))
                 and len(value) >= target)):
            raise ValueError('Items in {} should be greater than equal to than {}'.format(name, target))

    @staticmethod
    def check_max_items(name, value, target):
        if not ((isinstance(value, (list, MutableSequence)) or isinstance(value, (dict, MutableMapping))
                 and len(value) <= target)):
            raise ValueError('Items in {} should be less than equal to than {}'.format(name, target))

    @staticmethod
    def check_enum(name, value, target):
        if value not in target:
            raise ValueError('Possible value for {} should be one of {}'.format(name, target))

    @staticmethod
    def check_min_set_attributes(name, value, target):
        if hasattr(value, '__dict__'):
            count = 0
            for key, val in value.__dict__.items():
                if val is not None:
                    count += 1
                    if count >= target:
                        return
            if count < target:
                raise ValueError('Min set attributes in {} should be greater than equal to {}'.format(name, target))
        elif hasattr(value, '__slots__'):
            count = 0
            for key in value.__slots__:
                val = getattr(value, key)
                if val is not None:
                    count += 1
                    if count >= target:
                        return
            if count < target:
                raise ValueError('Min set attributes in {} should be greater than equal to {}'.format(name, target))
        else:
            raise ValueError('{} must be have a __dict__ or __slots__ attribute'.format(name))

    @staticmethod
    def check_subclass(name, value, target):
        if not issubclass(value, target):
            raise ValueError('{} should be a subclass of {}'.format(name, target))

    @staticmethod
    def check_required_set_attributes(name, value, target):
        for f in target:
            if getattr(value, f) is None:
                raise ValueError('Attribute "{}" of {} must be set and can not be None'.format(f, name))

    @staticmethod
    def check_atleast_one_set_attributes(name, value, target):
        for f in target:
            if getattr(value, f) is not None:
                return
        raise ValueError('At least one attribute in {} must be set from {}'.format(name, target))

    @staticmethod
    def check_attribute_value(name, value, target):
        for k, v in target.items():
            if getattr(value, k) != v:
                raise ValueError('attribute value for {} in {} must be equal to {}'.format(k, name, v))

    @staticmethod
    def check_all_set_attributes(name, value, target):
        if hasattr(value, '__dict__'):
            for key, val in value.__dict__.items():
                if val is None:
                    raise ValueError('All attributes in {} must be set'.format(name))
        elif hasattr(value, '__slots__'):
            for key in value.__slots__:
                if getattr(value, key) is None:
                    raise ValueError('All attributes in {} must be set'.format(name))
        else:
            raise ValueError('{} must be have a __dict__ or __slots__ attribute'.format(name))

    @staticmethod
    def check_datetime_format(name, value, target):
        datetime.strptime(value, target)

    @staticmethod
    def check_element_constraints(name, value, target):
        if isinstance(value, (list, MutableSequence)):
            for i in range(len(value)):
                target.validate('{}[{}]'.format(name, i), value[i])
        elif isinstance(value, (dict, MutableMapping)):
            for key, val in value.items():
                target.validate(key, val)
        else:
            raise ValueError('value must be a list or dict')

    @staticmethod
    def check_default(target, value):
        pass


class DTDataValidator:
    applyValidation: bool = True
    validators: dict = {
        'type': Validators.check_type,                                  # Check for type
        'lt': Validators.check_less_than,                               # Check for less than
        'gt': Validators.check_greater_than,                            # Check for greater than
        'le': Validators.check_less_than_equal,                         # Check for less than equal to
        'ge': Validators.check_greater_than_equal,                      # Check for less than equal to
        'et': Validators.check_equal,                                   # Check for equal to
        'ne': Validators.check_not_equal,                               # Check for not equal to
        'min_items': Validators.check_min_items,                        # Check for min no of items in list or dict
        'max_items': Validators.check_max_items,                        # Check for Max no of items in list or dict
        'enum': Validators.check_enum,                                  # Check for attribute value which should be one from enum[]
        'msa': Validators.check_min_set_attributes,                     # Check for minimum set (not None) attributes of a class
        'rsa': Validators.check_required_set_attributes,                # Check if all attributes in the required list are not None
        'osa': Validators.check_atleast_one_set_attributes,             # Check if at least one attribute in the list is not None
        'asa': Validators.check_all_set_attributes,                     # Check if all attributes of the class are not none
        'sc': Validators.check_subclass,                                # Check if a class is a subclass of another class
        'av': Validators.check_attribute_value,                         # Check if a sub-attribute of an attribute has a specific value
        'date_time_format': Validators.check_datetime_format,           # Check date time format for an attribute
        'element_constraints': Validators.check_element_constraints,    # Apply constraints on members of dict or list
        'default': Validators.check_default
    }

    @staticmethod
    def get_validator(name):
        out = DTDataValidator.validators.get(name, None)
        if out is None:
            raise NotImplementedError('No validator is implemented for "{}"'.format(name))
        else:
            return out

    @staticmethod
    def add_validator(name, func):
        if name in DTDataValidator.validators:
            raise Exception('validator with name {} already defined in the set of available validators, '
                            'please provide a different name'.format(name))
        DTDataValidator.validators[name] = func


# In case a decorator is needed in future for some use case
def validate(**kwargs):
    def internal(func):
        def executioner(*args):
            if len(args) > 1:
                value = args[1]
            else:
                value = args[0]
            name = kwargs.pop('name', 'Attribute')
            if DTDataValidator.applyValidation is True:
                for key, target in kwargs.items():
                    DTDataValidator.get_validator(key)(name, value, target)
            return func(*args)
        return executioner
    return internal
