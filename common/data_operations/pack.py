import argparse
import enum
import hashlib
import os
import shutil
import yaml


# Enum for size units
class SIZE_UNIT(enum.Enum):
    BYTES = 1
    KB = 2
    MB = 3
    GB = 4


def create_info_file(output_dir, sha_value, size, size_unit):
    output_txt = os.path.join(output_dir, 'info.txt')
    output_yml = os.path.join(output_dir, 'info.yml')
    size_string = str(size) + size_unit.name

    with open(output_txt, 'w') as f:
        f.write('size: {}\n'.format(size_string))
        f.write('checksum: {}\n'.format(sha_value))
    print('Created {}'.format(output_txt))

    with open(output_yml, 'w') as f:
        data = {
            'size': size_string,
            'checksum': sha_value
        }
        yaml.safe_dump(data, f)
    print('Created {}'.format(output_yml))


def convert_unit(size_in_bytes, unit):
    """
        Convert the size from bytes to other units like KB, MB or GB
    """
    if unit == SIZE_UNIT.KB:
        size = size_in_bytes / 1024
    elif unit == SIZE_UNIT.MB:
        size = size_in_bytes / (1024 * 1024)
    elif unit == SIZE_UNIT.GB:
        size = size_in_bytes / (1024 * 1024 * 1024)
    else:
        size = size_in_bytes

    return round(size, 5)


def get_file_size(file_name, size_type=SIZE_UNIT.BYTES):
    """
        Get file in size in given unit like KB, MB or GB
    """
    size = os.path.getsize(file_name)
    return convert_unit(size, size_type)


def sha256hash(filename):
    """
        Calculate SHA256 hash string for a given file
    """
    sha256_hash = hashlib.sha256()
    with open(filename, "rb") as f:
        # Read and update hash string value in blocks of 4K
        for byte_block in iter(lambda: f.read(4096), b""):
            sha256_hash.update(byte_block)

    return sha256_hash.hexdigest()


def create_archive(input_dir, datatool_name, version, tag, output_dir):
    """
    Create datatool archive from the input dataset

    :param input_dir: Path to input data directory
    :param datatool_name: Name of the datatool (used in naming the zip)
    :param version: Version of the datatool (used in naming the zip)
    :param tag: Tag for the datatool (used in naming the zip)
    :param output_dir: Output directory
    :return: None
    """
    os.makedirs(output_dir, exist_ok=True)
    archive_name = '{}_{}_{}'.format(datatool_name, version, tag)
    archive_path = os.path.join(output_dir, archive_name)
    shutil.make_archive(archive_path, 'zip', input_dir)
    archive_path = archive_path + '.zip'
    print('Successfully created archive at {}'.format(archive_path))

    sha256_hash = sha256hash(archive_path)
    print('Checksum of archive is: ', sha256_hash)

    size = get_file_size(archive_path, SIZE_UNIT.GB)
    print('Size of archive is: ', size, 'GB')

    create_info_file(output_dir, sha256_hash, size, SIZE_UNIT.GB)


def parse_args():
    parser = argparse.ArgumentParser(description="Package data into a versioned and tagged zip archive")
    parser.add_argument('--input-dir', '-i', required=True, help='Path to the directory containing the data')
    parser.add_argument('--datatool-name', '-n', required=True, help='Name of the datatool')
    parser.add_argument('--datatool-version', '-v', required=True, help="Version of datatool for packing")
    parser.add_argument('--datatool-tag', '-t', required=True, help="Tag for the datatool for packing")
    parser.add_argument('--output-dir', '-o', required=True, help='Path to output directory')
    args = parser.parse_args()
    return args


def main():
    args = parse_args()
    create_archive(args.input_dir, args.datatool_name, args.datatool_version, args.datatool_tag, args.output_dir)


if __name__ == '__main__':
    main()
