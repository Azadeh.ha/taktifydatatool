import random
import argparse
import shutil
import os
import inspect
import sys
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, cur_path + '/../../')
from datatool_api.models.DTDataset import DTDataset


class GetSubset:
    @staticmethod
    def run(input_dir, output_dir, with_sample_files, sample_count, mode='memory', sample_type='imageSamples'):
        os.makedirs(output_dir, exist_ok=True)
        old_dataset = DTDataset(name='old_dataset', operatingMode=mode).load_from_json(
            os.path.join(input_dir, 'dataset.json'), element_list=[sample_type, 'metadata'])
        new_dataset = DTDataset(name='new_dataset', operatingMode=mode)

        source = old_dataset.imageSamples
        target = new_dataset.imageSamples
        if sample_type == 'videoSamples':
            source = old_dataset.videoSamples
            target = new_dataset.videoSamples
        elif sample_type == 'audioSamples':
            source = old_dataset.audioSamples
            target = new_dataset.audioSamples

        if source.item_count() < sample_count:
            raise Exception('Required sample count is more than samples available in input json')

        indices = random.sample(range(source.item_count()), sample_count)
        sample_ids = list(source.keys())

        for index in indices:
            sample = source.pop(sample_ids[index])
            target.add(sample.id, sample)
            if with_sample_files is True:
                os.makedirs(os.path.join(output_dir, os.path.dirname(sample.samplePath)), exist_ok=True)
                shutil.copy(os.path.join(input_dir, sample.samplePath),
                            os.path.join(output_dir, os.path.dirname(sample.samplePath))
                            )

        if old_dataset.metadata is not None:
            new_dataset.metadata = old_dataset.metadata

        # Dump the new json
        new_dataset.to_json(os.path.join(output_dir, 'dataset.json'), validate_sample_paths=with_sample_files)


def main():
    parser = argparse.ArgumentParser(description='Create a subset of dataset.json')
    parser.add_argument('--input-dir', '-i', help='Input directory', required=True)
    parser.add_argument('--output-dir', '-o', help='Output directory', required=True)
    parser.add_argument('--operation-mode', '-om', choices=['memory', 'disk', 'ramdisk'], default='memory',
                        help='Operation mode to use for the datatool')
    parser.add_argument('--sample-type', '-t', choices=['imageSamples', 'videoSamples', 'audioSamples'],
                        default='imageSamples', help='Sample type to dump')
    parser.add_argument('--sample-count', '-s', help='Number of samples', default=100, type=int)
    parser.add_argument("--with-sample-files", '-w', dest='with_sample_files', action='store_true',
                        help='If sample files (images/videos/audio) also need to be copied along with the annotations')

    args = parser.parse_args()
    GetSubset.run(args.input_dir, args.output_dir, args.with_sample_files, args.sample_count, args.operation_mode,
                  args.sample_type)


if __name__ == '__main__':
    main()
