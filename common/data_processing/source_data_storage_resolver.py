from ..data_operations.remote_proxies.proxy_s3 import proxy_s3
from ..data_operations.remote_proxies.proxy_synology import proxy_synology
from ..data_operations.remote_proxies.proxy_local import proxy_local
from typing import List, Dict
from collections import defaultdict
import os


class SourceDataStorageResolver:
    def __init__(self, storage: str, storage_loc: str, storage_root: str,
                 datatool_name: str, datatool_version: str, datatool_tags: List[str],
                 tag_dependencies: Dict[str, str], output_dir: str, default_fallback_tag: str = 'default'):
        self.output_dir = output_dir
        self.storage = storage
        self.storage_root = storage_root
        self.default_fallback_tag = default_fallback_tag

        self.object_mapping = {}
        self.tag_mapping = defaultdict(list)
        self.download_log = {}

        os.makedirs(self.output_dir, exist_ok=True)

        for tag in datatool_tags:
            if tag not in tag_dependencies:
                tag_dependencies[tag] = self.default_fallback_tag

        if storage == 's3':
            self.proxy = proxy_s3(storage_root)
        elif storage == 'synology':
            self.proxy = proxy_synology(storage_loc, storage_root)
        elif storage == 'local':
            self.proxy = proxy_local(storage_root)
        else:
            raise Exception('Undefined storage, must be synology, s3 or local')

        for tag in datatool_tags:
            object_name = '{}_{}_{}.zip'.format(datatool_name, datatool_version, tag)
            object_dir = os.path.join(datatool_name, datatool_version, tag)
            if self.proxy.check_if_exists(os.path.join(object_dir, object_name)) is True:
                self.object_mapping[tag] = os.path.join(object_dir, object_name)
                self.tag_mapping[os.path.join(object_dir, object_name)].append(tag)
            else:
                object_name = '{}_{}_{}.zip'.format(datatool_name, datatool_version, tag_dependencies[tag])
                object_dir = os.path.join(datatool_name, datatool_version, tag_dependencies[tag])
                if (self.proxy.check_if_exists(os.path.join(object_dir, object_name)) is True or
                        (isinstance(self.proxy, proxy_local) and os.path.exists(self.storage_root))):
                    self.object_mapping[tag] = os.path.join(object_dir, object_name)
                    self.tag_mapping[os.path.join(object_dir, object_name)].append(tag)
                else:
                    raise Exception('Can not map the data location for tag: {}'.format(tag))

    def resolve(self, tag) -> str:
        object_path = self.object_mapping[tag]
        if object_path not in self.download_log:
            downloaded_path = self.proxy.download_package(self.output_dir, object_path)
            self.download_log[object_path] = downloaded_path

        print('Dataset path for tag {} resolved to: {}'.format(tag, self.download_log[object_path]))
        return self.download_log[object_path]

    def cleanup(self, tag):
        if self.storage in ['s3', 'synology']:
            object_path = self.object_mapping[tag]
            tag_list = self.tag_mapping[object_path]
            tag_list.remove(tag)
            if len(tag_list) == 0:
                os.remove(self.download_log[object_path])
                self.download_log.pop(object_path)
