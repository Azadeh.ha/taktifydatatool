"""
This is an example data loader for pytorch which uses the datatool API to load the datatool output JSON and read samples.
The example task considered is a regression task for 17 2d body key points

Input:
The input is a datatool output directory containing the dataset.json and samples directory

Assumptions are:
    - The datatool output contains annotations for 17 2d body key points
    - The samples are RGB images
    - The models needs 200 x 200 size input tensor which contains the cropped and rescaled body region
    - There are some missing key points in some samples and we need to consider only those samples having all 17 key points
"""


from torch.utils.data import Dataset
from torchvision.transforms import transforms
from PIL import Image
import os
import inspect
import sys
cur_path = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path.insert(0, os.path.join(cur_path, '..'))
from custom_dataset_model import DTDatasetCustom
from datatool_api.config.APIConfig import DTAPIConfig
from custom_base_types import MRLDatasetPupil
import json

class MRLDataset(Dataset):
    def __init__(self, data_dir: str, operating_mode: str = 'memory', **kwargs):
        """
        Instantiate the dataset instance

        :param data_dir: Directory containing the datatool output which contains the "sample_files" directory and "dataset.json" file
        :param operating_mode: Operating mode for the datatool api to handle the dataset, based on the data size, user can choose [memory, disk or ramdisk]
        :param kwargs: Additional keyword arguments
        """
        Dataset.__init__(self)
        self.data_dir = data_dir
        self.operating_mode = operating_mode
        self.kwargs = kwargs
        # No need to validate when loading back the API output
        DTAPIConfig.disable_validation()
        # Load the dataset.json using the datatool API
        self.dataset = DTDatasetCustom(name='input_dataset', operatingMode=self.operating_mode).load_from_json(
            os.path.join(data_dir, 'dataset.json'))
			
        self.usable_annotations = []

        for info_id, info in self.dataset.info.items():
            self.usable_annotations.append(info_id)

        # Example Transform to resize and normalize the images: Modify it to write a more complex transform
        self.transform = transforms.Compose([transforms.Resize(self.kwargs['model_input_size']),
                                             transforms.ToTensor(),
                                             transforms.Normalize(mean=self.kwargs['normalization']['mean'],
                                                                  std=self.kwargs['normalization']['std'])])

    def __len__(self):
        return len(self.usable_annotations)

    def __getitem__(self, index):
        # Get the image ID for the index
        annot_id = self.usable_annotations[index]
        print(annot_id)

        image_id = self.dataset.info.get(annot_id)




        # Read image for the sample
        img = Image.open(os.path.join(self.data_dir, 'sample_files', image_id.image_name)).convert(self.kwargs['image_type'])
        # Apply the transform on cropped image
        img = self.transform(img)

        # Return copped image tensor 
        return img, image_id.eye_state,image_id.image_name


def main():
    params = {
        'image_type': 'RGB',                # Image type that the model is going to take as input, in this case it is
                                            # 3 channel RGB
        'model_input_size': (120, 120),     # Width x Height of input tensor for the model
                                            # in loading candidate list
        'normalization': {                  # Normalization parameters for image
            'mean': [154.78, 118.57, 101.74],
            'std': [53.80, 48.19, 46.15]
        }
    }

    # TODO: Set by the user
    data_dir = '../../output/train'

    # Create dataset instance
    MRL_dataset = MRLDataset(data_dir=data_dir, operating_mode='memory', **params)
    for i in range(len(MRL_dataset)):
        print(MRL_dataset.__getitem__(i))


if __name__ == main():
    main()
